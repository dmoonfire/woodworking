module.exports = {
    trailingComma: "es5",
    tabWidth: 4,
    semi: true,
    overrides: [
        {
            files: "package.json",
            options: {
                tabWidth: 2,
            },
        },
    ],
};
