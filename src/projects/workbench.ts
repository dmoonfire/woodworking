import { feet, inches, Length, Measure } from "safe-units";
import { LumberSet } from "../LumberSet";
import * as table from "markdown-table";

/*
 * Break out the various related values for building this desk.
 */

// Common measurements.
const feet8 = Measure.of(8, feet);
const feet6 = Measure.of(6, feet);
const feet4 = Measure.of(4, feet);

// The width of the project should be six feet.
const width: Length = feet6;

// The width of the door to the basement is 31 inches, so we need to be
// underneath that.
const maximumWidth: Length = Measure.of(30, inches);

// The height of the work surface should be about 40 inches, which seems to be
// a comfortable height for me to work.
const topHeight: Length = Measure.of(40, inches);

// We want two inches overhang on both sides of the workbench for clamping.
const lengthOverhang: Length = Measure.of(2, inches);

// Likewise, we want two inches overhang on the front (but not the back).
const widthOverhang: Length = Measure.of(2, inches);

// How tall is the upper shelf along the back?
const upperShelfHeight: Length = Measure.of(6, inches);

/**
 * Keep track of our master lists.
 */
var sets: { [key: string]: LumberSet } = {};

/*
 * Most of the workspace is based on the top board. This is a number of two
 * inch thick boards, but the key ones are the back which match the 2x4 leg
 * coming up from the sides and the front which hangs over the edge. We don't
 * worry about the overhang on the sides because they are supported by six feet
 * of board, but the front needs to be anchored strongly enough that it wouldn't
 * break off.
 */
const surface = (sets.surface = new LumberSet());
const surfaceBackWidth = feet6.minus(lengthOverhang).minus(lengthOverhang);

surface.add2x4(surfaceBackWidth); // This is the mandatory back one.
surface.add2x4(width, 0);
surface.add2x6(width, 0);
surface.add2x8(width, 3);

const surfaceWidth = surface.totalWidth;

console.log("# Surface\n");
console.log("- maximum width:", maximumWidth.in(inches));
console.log("- width:", surfaceWidth.in(inches));
console.log("- cuts:", surface.cutsList.join(", "));

// Make sure we didn't exceed our width.
if (surfaceWidth.gt(maximumWidth)) {
    console.error("the surface is too wide");
}

/*
 * Figure out the dimensions of the frame underneath the surface. This is the
 * same dimensions as the surface, but with the overhangs build in. The long
 * pieces are the full width with four crossbars for structure.
 */

const surfaceFrame = (sets.surfaceFrame = new LumberSet());
const surfaceFrameLength = surfaceBackWidth;
const surfaceFrameWidth = surfaceWidth.minus(widthOverhang);

// The cross-lengths have the width of the wider pieces removed.
const surfaceFrameCrossLength = surfaceFrameWidth.minus(Measure.of(4, inches));

surfaceFrame.add2x4(surfaceFrameLength, 2);
surfaceFrame.add2x4(surfaceFrameCrossLength, 4);

console.log("surface frame:");
console.log("  cuts:", surfaceFrame.cutsList.join(", "));

/*
 * The lower shelf has the same dimensions as the frame, but is covered in lumber.
 */

const lowerShelf = (sets.lowerShelf = new LumberSet());
const lowerShelfWidth = surfaceFrameWidth;
const lowerShelfLength = surfaceFrameLength;

lowerShelf.add2x4(lowerShelfLength, 5);
lowerShelf.add2x6(lowerShelfLength, 1);

console.log("lowerShelf:");
console.log("  desired width:", lowerShelfWidth.in(inches));
console.log("  width:", lowerShelf.totalWidth.in(inches));
console.log("  cuts:", lowerShelf.cutsList.join(", "));

// Make sure we didn't exceed our width.
if (
    lowerShelf.totalWidth.value.toFixed(2) != lowerShelfWidth.value.toFixed(2)
) {
    console.error("the self is the wrong width");
}

/*
 * The lower shelf frame is identical to the surface frame.
 */

const lowerShelfFrame = (sets.lowerShelfFrame = LumberSet.clone(surfaceFrame));

console.log("lower shelf frame:");
console.log("  cuts:", lowerShelfFrame.cutsList.join(", "));

/*
 * The upper shelf uses the back legs for the height and consist of a 1x4 across
 * the top and a 1x6 on the back.
 */

const upperShelf = (sets.upperShelf = new LumberSet());

upperShelf.addDimension(width, Measure.of(1, inches), Measure.of(4, inches));
upperShelf.addDimension(width, Measure.of(1, inches), upperShelfHeight);

console.log("upper shelf:");
console.log("  cuts:", upperShelf.cutsList.join(", "));

/*
 * Add in the legs to the workbench.
 */

// The legs are 2x4s (which match the back shelf).
const legs = (sets.legs = new LumberSet());

// The front two legs tuck underneath the overheight and go to the bottom.
const frontHeight = topHeight.minus(Measure.of(2, inches));

// The back two are the full height plus the upper shelf height minus the depth
// of the upper shelf (1 inch). This is also why we require a 2x4 for the back
// board on the surface.
const backHeight = topHeight
    .plus(upperShelfHeight)
    .minus(Measure.of(1, inches));

legs.add2x4(frontHeight, 2);
legs.add2x4(backHeight, 2);

console.log("legs:");
console.log("  front:", frontHeight.in(inches));
console.log("  back:", backHeight.in(inches));
console.log("  cuts:", legs.cutsList.join(", "));

/*
 * Combine everything together.
 */

const merged = LumberSet.merge(sets);

console.log("\n# Final Lists");

// List all the cuts we need to do.
console.log("\n## Cuts\n");

let cuts = merged.cuts.map((x) => [
    x.count.toString(),
    x.toDimension(),
    x.length.in(feet),
]);

console.log(
    table([["Count", "Dimension", "Feet"]].concat(cuts), {
        align: ["r", "l", "r"],
    })
);

// Back them into the smallest pieces of lumber.
console.log("\n## Purchase\n");

let packed = merged
    .pack([feet8, feet6])
    .map((x) => [x.count.toString(), x.toDimension(), x.length.in(feet)]);

console.log(
    table([["Count", "Dimension", "Feet"]].concat(packed), {
        align: ["r", "l"],
    })
);
