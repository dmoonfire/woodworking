import { Length } from "safe-units";
import { Lumber } from "./Lumber";

export class CountedLumber extends Lumber {
    public constructor(
        public count: number,
        length: Length,
        depth: Length,
        width: Length
    ) {
        super(length, depth, width);
    }

    public toString(): string {
        return "(" + this.count + ") " + super.toString();
    }

    public toDimension(): string {
        return super.toString();
    }
}
