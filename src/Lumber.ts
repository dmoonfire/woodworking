import { inches, Length } from "safe-units";

export class Lumber {
    public constructor(
        public length: Length,
        public depth: Length,
        public width: Length
    ) {}

    public static clone(lumber: Lumber): Lumber {
        return new Lumber(lumber.length, lumber.depth, lumber.width);
    }

    public toString(): string {
        let results = [
            this.format(this.length),
            this.format(this.depth),
            this.format(this.width),
        ].join("x");

        return results;
    }

    private format(length: Length): string {
        let output = length.in(inches);

        output = output.replace(/ in$/, "");
        output = output.replace(/(\d+)\.(\d\d\d)\d+$/, "$1.$2");
        output = output.replace(/(\d+)\.0+$/, "$1");
        output = output.replace(/(\d+)\.9+$/, (_, m1) =>
            (parseInt(m1) + 1).toString()
        );

        return output;
    }
}
