import { inches, Length, Measure } from "safe-units";
import { CountedLumber } from "./CountedLumber";
import { Lumber } from "./Lumber";

export class LumberSet {
    public lumber: Lumber[] = [];

    public get cuts(): CountedLumber[] {
        // Create a unique list of lumber with a count of each one.
        let sum: { [size: string]: CountedLumber } = {};

        for (const piece of this.lumber) {
            const size = piece.toString();

            if (!sum[size]) {
                sum[size] = new CountedLumber(
                    1,
                    piece.length,
                    piece.depth,
                    piece.width
                );
            } else {
                sum[size].count++;
            }
        }

        // Gather up the sums and return them in an ordered list.
        let list: CountedLumber[] = [];

        for (const index in sum) {
            list.push(sum[index]);
        }

        list.sort((a, b) => a.toString().localeCompare(b.toString()));

        return list;
    }

    public get cutsList(): string[] {
        return this.cuts.map((x) => x.toString());
    }

    public get totalWidth(): Length {
        return this.lumber.reduce(
            (sum, lumber) => sum.plus(lumber.width),
            Measure.of(0, inches)
        );
    }

    public static clone(input: LumberSet): LumberSet {
        var output = new LumberSet();

        output.lumber = input.lumber.map((x) => x);

        return output;
    }

    public static merge(input: { [key: string]: LumberSet }): LumberSet {
        const results = new LumberSet();

        for (const index in input) {
            var set = input[index];

            for (const piece of set.lumber) {
                results.add(piece);
            }
        }

        return results;
    }

    public add(piece: Lumber): LumberSet {
        this.lumber.push(piece);
        return this;
    }

    public add2x4(length: Length, count: number = 1): LumberSet {
        return this.addDimension(
            length,
            Measure.of(2, inches),
            Measure.of(4, inches),
            count
        );
    }

    public add2x6(length: Length, count: number = 1): LumberSet {
        return this.addDimension(
            length,
            Measure.of(2, inches),
            Measure.of(6, inches),
            count
        );
    }

    public add2x8(length: Length, count: number = 1): LumberSet {
        return this.addDimension(
            length,
            Measure.of(2, inches),
            Measure.of(8, inches),
            count
        );
    }

    public addDimension(
        length: Length,
        depth: Length,
        width: Length,
        count: number = 1
    ): LumberSet {
        for (let i = 0; i < count; i++) {
            this.lumber.push(new Lumber(length, depth, width));
        }

        return this;
    }

    public pack(lengths: Length[]): CountedLumber[] {
        // Start by trying to pack each piece into the largest sizes.
        const first = lengths[0];
        const list: Lumber[] = [];

        // Go through each piece.
        for (const piece of this.lumber) {
            // First see if we have a piece that can fit this.
            var search = list.filter(
                (x) =>
                    x.length.plus(piece.length).lte(first) &&
                    x.depth.eq(piece.depth) &&
                    x.width.eq(piece.width)
            )[0];

            if (search) {
                // We found it, so add it to the list and move on.
                if (false) {
                    console.log(
                        "merged",
                        piece.length.in(inches),
                        "to",
                        search.length.in(inches),
                        "for",
                        search.length.plus(piece.length).in(inches)
                    );
                }

                search.length = search.length.plus(piece.length);
                continue;
            }

            // We didn't find it, so add a new piece.
            list.push(Lumber.clone(piece));
        }

        // Go through the list and find the small piece that will fit.
        const rev = lengths.reverse();

        for (const index in list) {
            const piece = list[index];
            var minimumSize = rev.filter((x) => x.gte(piece.length))[0];

            if (!minimumSize) {
                console.error("cannot find piece into measurements:", piece);
            } else {
                piece.length = minimumSize;
            }
        }

        // Now that we have a consolidated list, return the results.
        var merged = new LumberSet();

        for (const piece of list) {
            merged.add(piece);
        }

        return merged.cuts;
    }
}
